package com.example.weatherapp;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.weatherapp.data.City;
import com.example.weatherapp.data.WeatherDbHelper;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    int RCODE_NEW_CITY = 1;
    int RCODE_UPDATE_CITY = 2;
    WeatherDbHelper dbHelper = new WeatherDbHelper(this);
    SimpleCursorAdapter cursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ListView lv = (ListView) findViewById(R.id.cityList);


        dbHelper.populate();
        cursorAdapter = new SimpleCursorAdapter(this,
                R.layout.row,
                dbHelper.fetchAllCities () ,
                new String[] { WeatherDbHelper.COLUMN_ICON, WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.
                COLUMN_COUNTRY, WeatherDbHelper.COLUMN_TEMPERATURE },
                new int[] { R.id.imageViewRow, R.id.cName, R.id.cCountry, R.id.temperature});
        cursorAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                if(view.getId() == R.id.imageViewRow) {
                    String iconCode = cursor.getString(columnIndex);
                    String icon = "icon_"+ iconCode;
                    int iconToShow = getResources().getIdentifier(icon, "drawable", getPackageName());
                    ((ImageView) view).setImageResource(iconToShow);
                    return true;
                } else {
                    return false;
                }
            }
        });
        lv.setAdapter(cursorAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor item = (Cursor) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtra("City", dbHelper.cursorToCity(item));
                startActivityForResult(intent, RCODE_UPDATE_CITY);
            }
        });

        final SwipeRefreshLayout sfl = findViewById(R.id.sfl);
        sfl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ArrayList<City> cities = new ArrayList<>(cursorAdapter.getCount());
                for (int i=0;i<cursorAdapter.getCount();i++){
                    Cursor o = (Cursor) lv.getItemAtPosition(i);
                    cities.add(dbHelper.cursorToCity(o));
                }
                HttpCitiesUpdate hcu = new HttpCitiesUpdate(MainActivity.this, cities);
                hcu.execute();
                sfl.setRefreshing(false);
            }
        });
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Enregistrement d'une nouvelle ville", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                City newCity = new City();
                intent.putExtra("new_city", newCity);
                startActivityForResult(intent, RCODE_NEW_CITY);
            }
        });
    }

    public void updateAllCities(ArrayList<City> cities){
        for (City city : cities){
            dbHelper.updateCity(city);
        }
        cursorAdapter.changeCursor(dbHelper.fetchAllCities());
        cursorAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RCODE_NEW_CITY){
            City newcity = data.getParcelableExtra("city_added");
            dbHelper.addCity(newcity);
            cursorAdapter.changeCursor(dbHelper.fetchAllCities());
            cursorAdapter.notifyDataSetChanged();
        }
        else if (requestCode == RCODE_UPDATE_CITY) {
            City updatedCity = data.getParcelableExtra("updated_city");
            dbHelper.updateCity(updatedCity);
            cursorAdapter.changeCursor(dbHelper.fetchAllCities());
            cursorAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
