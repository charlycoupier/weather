package com.example.weatherapp;

import android.os.AsyncTask;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.example.weatherapp.data.City;
import com.example.weatherapp.webservice.JSONResponseHandler;
import com.example.weatherapp.webservice.WebServiceUrl;

public class HttpWeatherGet extends AsyncTask<Object, Integer, Object> {

    private City city;
    private CityActivity activity;

    public HttpWeatherGet (City cityToUpdate, CityActivity activity) {
        this.city = cityToUpdate;
        this.activity = activity;
    }
    @Override
    protected Object doInBackground(Object[] objects) {
        HttpURLConnection conn = null;
        try {
            URL url = WebServiceUrl.build(city.getName(), city.getCountry());
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(15000);
            conn.setReadTimeout(15000);
            InputStream in = conn.getInputStream();
            System.out.print(in);
            JSONResponseHandler jsh = new JSONResponseHandler(city);
            jsh.readJsonStream(in);
        }
        catch (Exception e) {
            System.out.print(e);
        }
        finally {
            conn.disconnect();
        }
        return city;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        activity.updateView();
    }
}

