package com.example.weatherapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.weatherapp.data.City;

public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        final Intent intent = getIntent();
        final City newCity = intent.getParcelableExtra("new_city");

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);
        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                newCity.setName(textName.getText().toString());
                newCity.setCountry(textCountry.getText().toString());
                intent.putExtra("city_added", newCity);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }


}
