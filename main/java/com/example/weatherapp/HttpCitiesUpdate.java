package com.example.weatherapp;

import android.os.AsyncTask;

import com.example.weatherapp.data.City;
import com.example.weatherapp.webservice.JSONResponseHandler;
import com.example.weatherapp.webservice.WebServiceUrl;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class HttpCitiesUpdate extends AsyncTask <City, Integer, City[]>{

    private MainActivity activity;
    private ArrayList<City> citiesToUpdate;

    public HttpCitiesUpdate(MainActivity activity, ArrayList<City> cities){
        this.activity = activity;
        this.citiesToUpdate = cities;
    }

    @Override
    protected City[] doInBackground(City...cities) {
        try {
            for (City city : citiesToUpdate) {
                URL url = WebServiceUrl.build(city.getName(), city.getCountry());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                JSONResponseHandler jsonResponseHandler = new JSONResponseHandler(city);
                jsonResponseHandler.readJsonStream(connection.getInputStream());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cities;
    }

    @Override
    protected void onPostExecute(City[] c) {
        super.onPostExecute(c);
        activity.updateAllCities(citiesToUpdate);
    }

}
